package com.zoke.screen.recording;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

/**
 * @author 大熊
 * @date 创建时间: 17/1/22
 * @email 651319154@qq.com
 */

public class RecordApplication extends Application {
    private static RecordApplication application;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        application = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // 启动 Marvel service
        startService(new Intent(this, RecordService.class));
    }


    public static RecordApplication getInstance() {
        return application;
    }
}
